// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "UnrealSFASGameInstance.generated.h"

// Used to store information among levels
UCLASS()
class UNREALSFAS_API UUnrealSFASGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
    int LastLevelHealth;

    int LevelCount = 0;
};
