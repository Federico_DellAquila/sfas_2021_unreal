// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "UnrealSFASISlowMo.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUnrealSFASISlowMo : public UInterface
{
	GENERATED_BODY()
};

class UNREALSFAS_API IUnrealSFASISlowMo
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SlowMoFunction")
    void EnableSlowMo(const bool Enabled);
};
