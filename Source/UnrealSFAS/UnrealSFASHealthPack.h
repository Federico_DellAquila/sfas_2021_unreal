// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UnrealSFASHealthPack.generated.h"

class UUnrealSFASDamagerComponent;
class USphereComponent;

UCLASS()
class UNREALSFAS_API AUnrealSFASHealthPack : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASHealthPack();

    UPROPERTY(EditDefaultsOnly)
    UStaticMeshComponent* CrossPieceVertical;

    UPROPERTY(EditDefaultsOnly)
    UStaticMeshComponent* CrossPieceHorizontal;

    UPROPERTY(EditDefaultsOnly)
    USphereComponent* SphereCollider;

    UPROPERTY(EditDefaultsOnly)
    UUnrealSFASDamagerComponent* DamagerComponent;
};
