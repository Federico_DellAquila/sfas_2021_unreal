// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealSFASDamagerComponent.h"

// Sets default values for this component's properties
UUnrealSFASDamagerComponent::UUnrealSFASDamagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

    bIsDamage = true;

	// ...
}


int UUnrealSFASDamagerComponent::GetDamageValue() const
{
    return DamageValue;
}

void UUnrealSFASDamagerComponent::SetDamageValue(const int Value)
{
    DamageValue = FMath::Clamp(Value, 0, 100);
}

void UUnrealSFASDamagerComponent::AddPushback(UStaticMeshComponent* TargetMesh, const FVector Direction, const float Magnitude)
{
    TargetMesh->SetAllPhysicsLinearVelocity(FVector(0.0f));

    FVector Impulse = Direction * Magnitude;
    TargetMesh->AddImpulse(Impulse, NAME_None, true);
}

// Called when the game starts
void UUnrealSFASDamagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUnrealSFASDamagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

