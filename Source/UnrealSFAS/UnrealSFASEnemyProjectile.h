// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealSFASBaseProjectile.h"
#include "UnrealSFASEnemyProjectile.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSFAS_API AUnrealSFASEnemyProjectile : public AUnrealSFASBaseProjectile
{
	GENERATED_BODY()

public:
    AUnrealSFASEnemyProjectile();	
};
