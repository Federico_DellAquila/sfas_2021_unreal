#include "UnrealSFASMazeExit.h"

#include "Kismet/GameplayStatics.h"

#include "UnrealSFASGameInstance.h"
#include "UnrealSFASGameState.h"
#include "UnrealSFASCharacter.h"
#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASPlayerHUD.h"
#include "UnrealSFASPlayerWidget.h"

// Sets default values
AUnrealSFASMazeExit::AUnrealSFASMazeExit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
    StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
    StaticMeshComponent->SetGenerateOverlapEvents(false);
    StaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

    StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AUnrealSFASMazeExit::OnOverlapBegin);

    FadeTime = 1.0f;
}

void AUnrealSFASMazeExit::OpenExit()
{
    // Change material to show that the exit is unlocked
    if (StaticMeshComponent)
    {
        StaticMeshComponent->SetGenerateOverlapEvents(true);

        if (OpenMaterial)
        {
            StaticMeshComponent->SetMaterial(0, OpenMaterial);
        }
    }
}

void AUnrealSFASMazeExit::NextLevel()
{
    // Increase the Level Counter in Game Instance and store the Player current health.
    UUnrealSFASGameInstance* GameInstance = Cast<UUnrealSFASGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
    if (GameInstance)
    {
        AUnrealSFASCharacter* PlayerRef = Cast<AUnrealSFASCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
        if (PlayerRef)
        {
            GameInstance->LastLevelHealth = PlayerRef->HealthComponent->GetHealthValue();
            GameInstance->LevelCount++;
        }
    }

    UGameplayStatics::OpenLevel(GetWorld(), FName(*GetWorld()->GetName()), false);
}

void AUnrealSFASMazeExit::BeginPlay()
{
    if (ClosedMaterial)
    {
        StaticMeshComponent->SetMaterial(0, ClosedMaterial);
    }

    UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(1.0f, 0.0f, FadeTime, FColor::Black, false, true);
}

void AUnrealSFASMazeExit::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    // If there are no enemies in the level, the player can move to the next level
    AUnrealSFASCharacter* Player = Cast<AUnrealSFASCharacter>(OtherActor);
    if (Player)
    {
        UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StartCameraFade(0.0f, 1.0f, FadeTime, FColor::Black, false, true);

        FTimerHandle Timer;
        GetWorldTimerManager().SetTimer(Timer, this, &AUnrealSFASMazeExit::NextLevel, FadeTime, false);
    }
}

