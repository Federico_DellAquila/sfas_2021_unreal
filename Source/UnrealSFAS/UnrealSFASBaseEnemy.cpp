// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASBaseEnemy.h"
#include "Kismet/GameplayStatics.h"

#include "UnrealSFASGameState.h"
#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASDamagerComponent.h"
#include "UnrealSFASGameMode.h"
#include "UnrealSFASDamagerComponent.h"
#include "UnrealSFASHealthPack.h"

// Sets default values
AUnrealSFASBaseEnemy::AUnrealSFASBaseEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    SetTickGroup(ETickingGroup::TG_PostPhysics);

    StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
    StaticMeshComponent->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));
    StaticMeshComponent->SetSimulatePhysics(true);
    StaticMeshComponent->SetEnableGravity(false);
    StaticMeshComponent->BodyInstance.bLockZRotation = true;
    StaticMeshComponent->BodyInstance.bLockXRotation = true;
    StaticMeshComponent->BodyInstance.bLockYRotation = true;
    StaticMeshComponent->BodyInstance.bLockXTranslation = true;
    StaticMeshComponent->SetNotifyRigidBodyCollision(true);
    StaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
    StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
    SetRootComponent(StaticMeshComponent);

    HealthComponent = CreateDefaultSubobject<UUnrealSFASHealthComponent>(TEXT("HealthComponent"));

    DamagerComponent = CreateDefaultSubobject<UUnrealSFASDamagerComponent>(TEXT("DamagerComponent"));
    DamagerComponent->SetDamageValue(10);

    if (StaticMeshComponent)
    {
        StaticMeshComponent->OnComponentHit.AddDynamic(this, &AUnrealSFASBaseEnemy::OnContact);
    }

    GameState = Cast<AUnrealSFASGameState>(UGameplayStatics::GetGameState(AActor::GetWorld()));
    if (GameState->IsValidLowLevel())
    {
        GameState->OnEnableActorsSlowMo_Delegate.AddDynamic(this, &AUnrealSFASBaseEnemy::EnableSlowMo);
    }

    HealthPackDropChance = 5;
}

void AUnrealSFASBaseEnemy::BeginPlay()
{
    Super::BeginPlay();

    StaticMeshComponent->SetWorldRotation(FRotator(0.0f));
}

void AUnrealSFASBaseEnemy::Tick(float DeltaSeconds)
{
    // Adjust DeltaSeconds based on Time Dilation
    if (GameState)
    {
        AdjustedDeltaSeconds = DeltaSeconds * GameState->InvertedGlobalTimeDilationValue;
    }

    Super::Tick(AdjustedDeltaSeconds);
}

void AUnrealSFASBaseEnemy::Destroyed()
{
    // When destroyed
    UWorld* World = GetWorld();
    if (World)
    {
        AUnrealSFASGameMode* GameMode = Cast<AUnrealSFASGameMode>(UGameplayStatics::GetGameMode(World));
        if (GameMode)
        {   
            // Remove from enemies list
            GameMode->RemoveEnemy(this);
        }
    }

    // Is the Health Pack class is selected
    if (HealthPackClass)
    {
        // Determine the drop chance
        int RandomValue = FMath::RandRange(0, 100);
        if (RandomValue <= HealthPackDropChance)
        {
            //Spawn a Health Pack
             FVector Loc = GetActorLocation();
             FRotator Rot = FRotator(0.0f);
             FVector Scale = FVector(1.0f, 1.0f, 1.0f);
             FTransform Transf = FTransform(Rot, Loc, Scale);
             FActorSpawnParameters Parameters;
             GetWorld()->SpawnActor<AUnrealSFASHealthPack>(HealthPackClass, Transf, Parameters);
        }
    }

    Super::Destroyed();
}

void AUnrealSFASBaseEnemy::EnableSlowMo_Implementation(const bool Enabled)
{
    StaticMeshComponent->SetPhysicsLinearVelocity(FVector(0.0f));
}

void AUnrealSFASBaseEnemy::OnContact(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
}