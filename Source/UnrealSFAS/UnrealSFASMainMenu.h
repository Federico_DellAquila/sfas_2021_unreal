// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UnrealSFASMainMenu.generated.h"

class UCanvasPanel;
class USizeBox;
class UScaleBox;
class UTextBlock;
class UVerticalBox;
class UButton;
class UImage;

UCLASS()
class UNREALSFAS_API UUnrealSFASMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:
    UUnrealSFASMainMenu(const FObjectInitializer& ObjectInitializer);

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UCanvasPanel* MainMenuCanvasPanel;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UImage* BlackScreen;

    ///////////////////////////////////////////////////////////////
    // Buttons
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* ButtonsSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* ButtonsScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UVerticalBox* ButtonsVerticalBox;

    ///////////////////////////////////////////////////////////////
    // Start Button
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* StartButtonSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* StartButtonScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UButton* StartButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* StartButtonTextBlock;

    ///////////////////////////////////////////////////////////////
    // Quit Button
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* QuitButtonSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* QuitButtonScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UButton* QuitButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* QuitButtonTextBlock;

    ///////////////////////////////////////////////////////////////
    // Credits
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* CreditsSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* CreditsScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* CreditsTextBlock;

    void NativeConstruct() override;

    virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

private:
    UFUNCTION()
    void StartGame();

    UFUNCTION()
    void QuitGame();

    UFUNCTION()
    void OpenNewLevel();

    bool bIsFading;

    float CurrentTimer;

    float MaxTimer;
};
