// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASBaseProjectile.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"

#include "UnrealSFASDamagerComponent.h"
#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASGameState.h"

// Sets default values
AUnrealSFASBaseProjectile::AUnrealSFASBaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // Very importat to avoid unwanted "blink" when enabling Slow Mo
    SetTickGroup(ETickingGroup::TG_PostPhysics);

    StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
    StaticMeshComponent->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));
    StaticMeshComponent->SetSimulatePhysics(true);
    StaticMeshComponent->SetGenerateOverlapEvents(true);
    StaticMeshComponent->SetNotifyRigidBodyCollision(true);
    StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    StaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    SetRootComponent(StaticMeshComponent);

    DamagerComponent = CreateDefaultSubobject<UUnrealSFASDamagerComponent>(TEXT("DamagerComponent"));
    DamagerComponent->SetDamageValue(34);

    Speed = 2000.0f;

    GameState = Cast<AUnrealSFASGameState>(UGameplayStatics::GetGameState(AActor::GetWorld()));
    if (GameState->IsValidLowLevel())
    {
        GameState->OnEnableActorsSlowMo_Delegate.AddDynamic(this, &AUnrealSFASBaseProjectile::EnableSlowMo);
    }

    if (StaticMeshComponent)
    {
        StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AUnrealSFASBaseProjectile::OnOverlapBegin);
    }
}

// Called when the game starts or when spawned
void AUnrealSFASBaseProjectile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AUnrealSFASBaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    // Move forward undefintely
    FVector Direction = GetActorForwardVector();

    // Adjust velocity based on Global Time Dilation
    FVector LocVelocity = Direction * Speed * GameState->InvertedGlobalTimeDilationValue;
    StaticMeshComponent->SetAllPhysicsLinearVelocity(LocVelocity);
}

void AUnrealSFASBaseProjectile::EnableSlowMo_Implementation(const bool Enabled)
{
    StaticMeshComponent->SetPhysicsLinearVelocity(FVector(0.0f));
}

void AUnrealSFASBaseProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
            int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    UUnrealSFASHealthComponent* OtherHealthComponent = Cast<UUnrealSFASHealthComponent>(OtherActor->GetComponentByClass(UUnrealSFASHealthComponent::StaticClass()));
    if (OtherHealthComponent)
    {
        if (OtherHealthComponent->GetCanBeDamaged())
        {
            if (HitFX)
            {
                UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, GetActorLocation(), FRotator(), true);
            }
        }
    }
    this->Destroy();
}