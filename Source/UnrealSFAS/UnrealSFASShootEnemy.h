// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealSFASBaseEnemy.h"
#include "UnrealSFASShootEnemy.generated.h"

class AUnrealSFASBaseProjectile;

UCLASS()
class UNREALSFAS_API AUnrealSFASShootEnemy : public AUnrealSFASBaseEnemy
{
	GENERATED_BODY()

public:
    AUnrealSFASShootEnemy();

    virtual void BeginPlay() override;

    virtual void Tick(float DeltaSeconds) override;

    UPROPERTY(EditDefaultsOnly, Category = "Combat")
    TSubclassOf<AUnrealSFASBaseProjectile> Projectile;

    // Define the range for Player detection
    UPROPERTY(EditDefaultsOnly, Category = "Combat")
    float DetectionRadius;

    UFUNCTION()
    void ShootProjectile();

    UPROPERTY(VisibleAnywhere, Category = "Combat");
    float CurrentCooldown;

    UPROPERTY(EditDefaultsOnly, Category = "Combat");
    float MaxCooldown;
};
