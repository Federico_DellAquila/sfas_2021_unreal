// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "UnrealSFASISlowMo.h"

#include "UnrealSFASBaseProjectile.generated.h"

class UUnrealSFASDamagerComponent;
class AUnrealSFASGameState;
class UParticleSystem;

UCLASS()
class UNREALSFAS_API AUnrealSFASBaseProjectile : public AActor, public IUnrealSFASISlowMo
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASBaseProjectile();

    UPROPERTY(EditDefaultsOnly, Category = "StaticMesh")
    UStaticMeshComponent* StaticMeshComponent;

    UPROPERTY(EditDefaultsOnly, Category = "Combat")
    UUnrealSFASDamagerComponent* DamagerComponent;

    UPROPERTY(EditDefaultsOnly, Category = "FX")
    UParticleSystem* HitFX;

    UPROPERTY()
    AUnrealSFASGameState* GameState;

    UPROPERTY(EditDefaultsOnly, Category = "Movements")
    float Speed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SlowMoFunction")
    void EnableSlowMo(const bool Enabled);
    virtual void EnableSlowMo_Implementation(const bool Enabled) override;

    UFUNCTION()
    virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};