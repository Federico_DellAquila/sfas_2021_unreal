#pragma once

UENUM()
enum class EEnemyType
{
    BOUNCER = 0    UMETA(DisplayName = "BOUNCER"),
    TURRET = 1    UMETA(DisplayName = "TURRET")
};
