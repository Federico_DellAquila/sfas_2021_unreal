#include "UnrealSFASEnemyProjectile.h"

#include "Kismet/GameplayStatics.h"

#include "UnrealSFASCharacter.h"
#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASDamagerComponent.h"

AUnrealSFASEnemyProjectile::AUnrealSFASEnemyProjectile()
    : Super()
{
    DamagerComponent->SetDamageValue(8);
    Speed = 1500.0f;

    StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);
}
