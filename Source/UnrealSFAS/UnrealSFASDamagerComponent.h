// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UnrealSFASDamagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNREALSFAS_API UUnrealSFASDamagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUnrealSFASDamagerComponent();

    int GetDamageValue() const;
    void SetDamageValue(const int Value);

    void AddPushback(UStaticMeshComponent* TargetMesh, const FVector Direction, const float Magnitude);

    UPROPERTY(EditDefaultsOnly)
    bool bIsDamage;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

    UPROPERTY(EditAnywhere, Category = "Damage");
    int DamageValue;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
