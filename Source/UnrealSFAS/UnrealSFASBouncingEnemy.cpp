// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASBouncingEnemy.h"

#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASDamagerComponent.h"
#include "UnrealSFASGameState.h"

AUnrealSFASBouncingEnemy::AUnrealSFASBouncingEnemy()
    : Super()
{
    PrimaryActorTick.bCanEverTick = true;
    Speed = 100.0f;
}

void AUnrealSFASBouncingEnemy::BeginPlay()
{
    Super::BeginPlay();
    SelectNewMovementDirection();
}

void AUnrealSFASBouncingEnemy::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    // Update movement the velocity based on the Global Time Dilation
    FVector LocVelocity = MovementDirection * Speed * GameState->InvertedGlobalTimeDilationValue;
    StaticMeshComponent->SetAllPhysicsLinearVelocity(LocVelocity);
}

void AUnrealSFASBouncingEnemy::SelectNewMovementDirection()
{
    // Select a random new direction
    uint32 RandomValue = FMath::RandRange(0, 3);
    switch (RandomValue)
    {
        case 0:
            MovementDirection = FVector(0.0f, 1.0f, 1.0f);
            break;
        case 1:
            MovementDirection = FVector(0.0f, 1.0f, -1.0f);
            break;
        case 2:
            MovementDirection = FVector(0.0f, -1.0f, 1.0f);
            break;
        case 3:
            MovementDirection = FVector(0.0f, -1.0f, -1.0f);
            break;
    }
}

void AUnrealSFASBouncingEnemy::OnContact(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    // When hitting something, change direction
    SelectNewMovementDirection();
}
