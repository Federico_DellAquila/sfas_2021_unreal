// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASPlayerProjectile.h"

#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"

#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASDamagerComponent.h"

AUnrealSFASPlayerProjectile::AUnrealSFASPlayerProjectile()
    : Super()
{
    StaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    StaticMeshComponent->SetWorldScale3D(FVector(0.51f, 0.09f, 0.09f));

    CapsuleCollider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollider"));
    CapsuleCollider->SetGenerateOverlapEvents(true);
    CapsuleCollider->SetEnableGravity(false);
    CapsuleCollider->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    CapsuleCollider->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    CapsuleCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
    CapsuleCollider->SetupAttachment(RootComponent);

    CapsuleCollider->OnComponentBeginOverlap.AddDynamic(this, &AUnrealSFASPlayerProjectile::OnOverlapBegin);

    Speed = 3500.0f;
}

void AUnrealSFASPlayerProjectile::BeginPlay()
{
    Super::BeginPlay();
}

void AUnrealSFASPlayerProjectile::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void AUnrealSFASPlayerProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    UUnrealSFASHealthComponent* OtherHealthComponent = OtherActor->FindComponentByClass<UUnrealSFASHealthComponent>();
    if (OtherHealthComponent)
    {
        if (OtherHealthComponent->GetCanBeDamaged() == true)
        {
            OtherHealthComponent->ApplyDamage(DamagerComponent->GetDamageValue());

            if (HitFX)
            {
                UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, GetActorLocation(), FRotator(), true);
            }
        }
    }
    this->Destroy();
}
