// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASMaze.h"
#include "Misc/DateTime.h"
#include "Kismet/GameplayStatics.h"

#include "UnrealSFASGameMode.h"
#include "UnrealSFASBaseEnemy.h"
#include "UnrealSFASMazeExit.h"

// Sets default values
AUnrealSFASMaze::AUnrealSFASMaze()
{
 	// No need to tick the maze at this point
	PrimaryActorTick.bCanEverTick = false;
    EnemyPercentage = 0.035f;
}

// Called when the game starts or when spawned
void AUnrealSFASMaze::BeginPlay()
{
	Super::BeginPlay();

	GenerateLevel();

    AUnrealSFASGameMode* GameMode = Cast<AUnrealSFASGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    GameMode->CurrentMaze = this;
}

FString AUnrealSFASMaze::Uint64ToBinary(uint64 value)
{
	// https://stackoverflow.com/questions/22746429/c-decimal-to-binary-converting

	FString binary;
	while (value != 0)
	{
		binary = (value % 2 == 0 ? "0" : "1") + binary;
		value /= 2;
	}
	return binary;
}

void AUnrealSFASMaze::GenerateMazeArray()
{
    // Set the Pseudo Random Generator seed
    pseudoRandomGenerator.seed(levelSeed);

    // A Distribution used to select among 4 possible directions
    std::uniform_int_distribution<uint32> distribution(0, 3);

    // Create a Random Walker
    FRandomWalker randomWalker(mazeHeight, mazeWidth);

    // Make it walk around in random directions 
    uint64 counter = 0;
    uint64 maxCounter = mazeHeight * mazeWidth;
    while (counter < maxCounter)
    {
        if (GetCellValue(randomWalker.positionY, randomWalker.positionX) == 1)
        {
            // For any cell it hit, set it's flag to 0
            SetCellValue(randomWalker.positionY, randomWalker.positionX, 0);
            TotalEmptySpaces++;
        }
        // Choose a random direction and move the Walker using the uniform int distribution
        EDirection direction = static_cast<EDirection>(distribution(pseudoRandomGenerator));
        randomWalker.Move(direction);

        counter++;
    }

    // Fill Maze Array Visual
    for (size_t i = 0; i < mazeWidth; ++i)
    {
        mazeArrayVisual.Add(Uint64ToBinary(mazeArray[i]));
    }
}

void AUnrealSFASMaze::GenerateSeed()
{
    if (bGenerateRandomSeed == true)
    {
        // Generate a seed from the hash of the current Date and Time
        FDateTime currentDateAndTime = FDateTime::Now();
        levelSeed = GetTypeHash(currentDateAndTime);
    }
}

void AUnrealSFASMaze::ClearMaze()
{
    mazeArray.Reset();
    for (UStaticMeshComponent* meshComp : staticMeshList) { meshComp->DestroyComponent(); }
    staticMeshList.Reset();

    // Calculate the biggest value using the row size to set every flag to 1
    for (size_t i = 0; i < mazeWidth; ++i)
    {
        uint64 maxPossibleBinaryValue = FMath::RoundUpToPowerOfTwo64(FMath::Pow(2, mazeHeight)) - 1;
        mazeArray.Add(maxPossibleBinaryValue);
    }

    for (AUnrealSFASBaseEnemy* enemy : EnemiesList) 
    { 
        enemy->Destroy();
    }
    EnemiesList.Reset();

    mazeArrayVisual.Reset();
}

void AUnrealSFASMaze::SpawnEnemies()
{
    // Find a random location in the maze
    std::uniform_int_distribution<uint32> DistributionZ(0, mazeHeight - 1);
    std::uniform_int_distribution<uint32> DistributionY(0, mazeWidth - 1);

    // Probability to spawn different enemies
    std::uniform_int_distribution<uint32> EnemyDistribution(0, EnemyTypes.Num() - 1);

    // Base on the free space, claculate how many enemies to spawn
    uint32 counter = 0;
    uint32 maxCounter = TotalEmptySpaces * EnemyPercentage;
    while (counter < maxCounter)
    {
        const uint32 PosZ = DistributionZ(pseudoRandomGenerator);
        const uint32 PosY = DistributionY(pseudoRandomGenerator);

        // If the are is empty
        if (CheckIfAreaEmpty(PosZ, PosY, 1))
        {
            // Get the location of the cell and spawn an enemy
            FVector Loc = GetCellPosition(PosZ, PosY);
            FQuat Rot(FVector(0.0f, 0.0f, 1.0f), 0.0f);
            FTransform Transf(Rot, Loc, FVector(1.0f, 1.0f, 1.0f));
            FActorSpawnParameters SpawnInfo;

            uint32 RandomEnemy = EnemyDistribution(pseudoRandomGenerator);
            UClass* EnemyClass = EnemyTypes[RandomEnemy];
            AUnrealSFASBaseEnemy* NewEnemy = GetWorld()->SpawnActor<AUnrealSFASBaseEnemy>(EnemyClass, Transf, SpawnInfo);
            EnemiesList.Add(NewEnemy);

            counter++;
        }
    }
}

void AUnrealSFASMaze::GenerateLevel()
{
	if (WallMesh)
	{
        ClearMaze();

        GenerateSeed();

        GenerateMazeArray();

		float yPos = 0.0f;
		float zPos = 0.0f;
		FQuat worldRotation(FVector(0.0f, 0.0f, 1.0f), 0.0f);
		FVector worldScale(blockHeight, blockWidth, blockWidth);
		uint64 mazeRow;

        uint64 lowestRow = 0;
        uint64 highestRow = 0;
        uint64 lowestIndex = 0;
        uint64 highestIndex = 0;

		// Loop through the binary values to generate the maze as static mesh components attached to the root of this actor
        for (size_t i = 0; i < mazeHeight; i++)
		{
			mazeRow = mazeArray[i];

            // Store lowest non-full walls row of the maze
            uint64 FullWallsRow = FMath::RoundUpToPowerOfTwo64(FMath::Pow(2, mazeHeight)) - 1;
            if (mazeRow != FullWallsRow && lowestRow == 0)
            {
                lowestRow = mazeRow;
                lowestIndex = i;
            }

            // Store highest non-full walls row of the maze
            if (mazeRow != FullWallsRow)
            {
                highestRow = mazeRow;
                highestIndex = i;
            }

			for (size_t j = 0; j < mazeWidth; j++)
			{
                bool bShouldBeWall = GetCellValue(i, j);

                // Spawn a wall if the cell flag is 1
				if (bShouldBeWall)
				{
					UStaticMeshComponent* meshComponent = NewObject<UStaticMeshComponent>(this);
					staticMeshList.Add(meshComponent);
					FVector worldPosition = GetCellPosition(i, j);
					FTransform worldXForm(worldRotation, worldPosition, worldScale);

					meshComponent->SetStaticMesh(WallMesh);
					meshComponent->SetWorldTransform(worldXForm);
                    meshComponent->SetCastShadow(false);            // Saves a loooooooot of performance
                    meshComponent->SetGenerateOverlapEvents(true);
                    meshComponent->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
					meshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);
					meshComponent->RegisterComponent();
				}
			}
		}

        // Generate a random location for the Player Start on the lowest non-full walls row
        int randomValue = -1;
        std::uniform_int_distribution<int> distribution(1, mazeHeight - 1);

        bool bIsWall = true;
        while (bIsWall == true)
        {
            // Find the first non-wall space in the lowest row
            randomValue = distribution(pseudoRandomGenerator);
            bIsWall = GetCellValue(lowestIndex, randomValue);
        }
        // Store the Starting Location
        StartLocation = GetCellPosition(lowestIndex, randomValue);

        // Generate a random location for the Maze Exit on the highest non-full walls row
        bIsWall = true;
        while (bIsWall == true)
        {
            // Find the first non-wall space in the highest row
            randomValue = distribution(pseudoRandomGenerator);
            bIsWall = GetCellValue(highestIndex, randomValue);
        }
        // Store the location for the Maze Exit
        EndLocation = GetCellPosition(highestIndex, randomValue);

        if (MazeExit)
        {
            // Move the Maze Exit to the location
            MazeExit->SetActorLocation(EndLocation);
        }

        // Spawn enemies in the empty spaces
        SpawnEnemies();
	}
}

bool AUnrealSFASMaze::GetCellValue(const uint32 Height, const uint32 Width) const
{
    check(Height <= mazeHeight && Width <= mazeWidth);

    const uint64 selectedRow = mazeArray[Height];
    const uint64 offset = (mazeWidth - (Width + 1));
    const uint64 mazeValue = (selectedRow >> offset);

    return (mazeValue & 1UI64);
}

bool AUnrealSFASMaze::CheckIfAreaEmpty(const uint32 PosZ, const uint32 PosY, const uint32 LayersNum) const
{
    return !GetCellValue(PosZ, PosY);
}

void AUnrealSFASMaze::SetCellValue(const uint32 Height, const uint32 Width, const bool Value)
{
    check(Height <= mazeHeight && Width <= mazeWidth);
    
    uint64& selectedRow = mazeArray[Height];
    const uint64 offset = (mazeWidth - (Width + 1));
    selectedRow = Value ? (selectedRow | (1UI64 << offset)) : (selectedRow & ~(1UI64 << offset));
}

FVector AUnrealSFASMaze::GetCellPosition(const uint32 Height, const uint32 Width) const
{
    check(Height <= mazeHeight && Width <= mazeWidth);

    const float yPos = GetActorLocation().Y + static_cast<float>(Width - (static_cast<float>(mazeWidth) / 2.0f)) * blockSize;
    const float zPos = GetActorLocation().Z + static_cast<float>(Height - (static_cast<float>(mazeHeight) / 2.0f)) * blockSize;

    return FVector(0.0f, yPos, zPos);
}

void AUnrealSFASMaze::OpenExit()
{
    MazeExit->OpenExit();
}
