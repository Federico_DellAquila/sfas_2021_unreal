// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASPlayerWidget.h"

// Engine Includes
#include "Runtime/UMG/Public/UMG.h"
#include "GenericPlatform/GenericPlatformMisc.h"

// Project Includes
#include "UnrealSFASCharacter.h"
#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASGameMode.h"
#include "UnrealSFASGameState.h"
#include "UnrealSFASGameInstance.h"

UUnrealSFASPlayerWidget::UUnrealSFASPlayerWidget(const FObjectInitializer& ObjectInitializer)
    : UUserWidget(ObjectInitializer)
{
    bIsPause = false;
}

void UUnrealSFASPlayerWidget::NativeConstruct()
{
    Super::NativeConstruct();

    PlayerRef = Cast<AUnrealSFASCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

    SetScreenEffectColor(FColor::Transparent);

    UpdateEnemiesNumber();
    UpdateShieldBar();
    UpdateWeaponBar();
    UpdateHealthBar();
    SetLevelCounter();

    MainMenuButton->OnPressed.AddDynamic(this, &UUnrealSFASPlayerWidget::MainMenu);
    QuitButton->OnPressed.AddDynamic(this, &UUnrealSFASPlayerWidget::Quit);
    ResumeButton->OnPressed.AddDynamic(this, &UUnrealSFASPlayerWidget::Resume);
}

void UUnrealSFASPlayerWidget::SetDrawBurstLine(const bool Value)
{
    bDrawBurstLine = Value;

    FColor TempColor = Value ? FColor::Cyan : FColor::Transparent;
    SetScreenEffectColor(TempColor);
}

void UUnrealSFASPlayerWidget::SetScreenEffectColor(const FColor Color)
{
    FSlateColor TempSlateColor{FLinearColor{Color}};
    ScreenEffectImage->SetBrushTintColor(TempSlateColor);
}

void UUnrealSFASPlayerWidget::OpenMinimap()
{
    MinimapSizeBox->SetWidthOverride(950.0f);
    MinimapSizeBox->SetHeightOverride(950.0f);
    MinimapImage->SetOpacity(0.85f);
}

void UUnrealSFASPlayerWidget::CloseMinimap()
{
    MinimapSizeBox->SetWidthOverride(350.0f);
    MinimapSizeBox->SetHeightOverride(350.0f);
    MinimapImage->SetOpacity(0.5f);
}

void UUnrealSFASPlayerWidget::UpdateEnemiesNumber()
{
    UWorld* World = GetWorld();
    if (World)
    {
        AUnrealSFASGameMode* GameMode = Cast<AUnrealSFASGameMode>(UGameplayStatics::GetGameMode(World));
        if (GameMode)
        {
            if (GameMode->GetEnemiesNumber() != 0)
            {
                FString CurrentNumberOfEnemis = FString("Enemies left: ");
                CurrentNumberOfEnemis.Append(FString::FromInt(GameMode->GetEnemiesNumber()));
                CurrentNumberOfEnemis.Append(FString(" / "));
                CurrentNumberOfEnemis.Append(FString::FromInt(GameMode->GetStartingEnemiesNumber()));

                FText EnemiesNumberText = FText::AsCultureInvariant(CurrentNumberOfEnemis);
                EnemyCounterTextBlock->SetText(EnemiesNumberText);
            }
            else
            {
                FString FinishedMazeString = FString("Look for the exit!");
                FText FinishedMazeText = FText::AsCultureInvariant(FinishedMazeString);
                EnemyCounterTextBlock->SetText(FinishedMazeText);
            }
        }
    }
}

void UUnrealSFASPlayerWidget::UpdateWeaponBar()
{
    if (PlayerRef)
    {
        const float LinearValue = FMath::Clamp(PlayerRef->CurrentWeaponEnergy / PlayerRef->MaxWeaponEnergy, 0.0f, 1.0f);
        WeaponProgressBar->SetPercent(LinearValue);
    }
}

void UUnrealSFASPlayerWidget::UpdateShieldBar()
{
    if (PlayerRef)
    {
        const float LinearValue = FMath::Clamp(PlayerRef->CurrentShieldEnergy / PlayerRef->MaxShieldEnergy, 0.0f, 1.0f);
        ShieldProgressBar->SetPercent(LinearValue);
    }
}

void UUnrealSFASPlayerWidget::UpdateHealthBar()
{
    if (PlayerRef)
    {
        UUnrealSFASHealthComponent* HealthComponent = PlayerRef->HealthComponent;
        if (HealthComponent)
        {
            const float LinearValue = static_cast<float>(HealthComponent->GetHealthValue()) / static_cast<float>(HealthComponent->GetMaxHealthValue());
            HealthProgressBar->SetPercent(LinearValue);
        }
    }
}

void UUnrealSFASPlayerWidget::EnablePauseMenu(const bool Value, const bool bIsDead)
{
    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    if (PlayerController)
    {
        AUnrealSFASGameState* GameState = Cast<AUnrealSFASGameState>(UGameplayStatics::GetGameState(GetWorld()));
        if (GameState)
        {
            if (Value == true && bIsPause == false)
            {
                GameplayUICanvasPanel->SetVisibility(ESlateVisibility::Collapsed);
                PauseMenuCanvasPanel->SetVisibility(ESlateVisibility::Visible);
                GameState->EnablePause(true);
                bIsPause = true;

                // Hide Resume button if this as been opened when the Player dies
                if (bIsDead)
                {
                    ResumeButtonSizeBox->SetVisibility(ESlateVisibility::Collapsed);
                }
                else
                {
                    ResumeButtonSizeBox->SetVisibility(ESlateVisibility::Visible);
                }
            }
            else if ((Value == false && bIsPause == true))
            {
                GameplayUICanvasPanel->SetVisibility(ESlateVisibility::Visible);
                PauseMenuCanvasPanel->SetVisibility(ESlateVisibility::Collapsed);
                GameState->EnablePause(false);
                bIsPause = false;
            }
        }
    }
}

void UUnrealSFASPlayerWidget::Quit()
{
    FGenericPlatformMisc::RequestExit(false);
}

void UUnrealSFASPlayerWidget::Resume()
{
    EnablePauseMenu(false);
}

void UUnrealSFASPlayerWidget::MainMenu()
{
    UGameplayStatics::OpenLevel(GetWorld(), "MainMenu", false);

    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    if (PlayerController)
    {
        PlayerController->SetPause(false);
        PlayerController->SetInputMode(FInputModeGameAndUI());

        // Reset level counter when the Player goes back to Main Menu
        UUnrealSFASGameInstance* GameInstance = Cast<UUnrealSFASGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
        if (GameInstance)
        {
            GameInstance->LevelCount = 0;
        }
    }
}

void UUnrealSFASPlayerWidget::SetLevelCounter()
{
    // This is called whenever the Player move to the next Level to update the Level Counter
    UUnrealSFASGameInstance* GameInstance = Cast<UUnrealSFASGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
    if (GameInstance)
    {
        FString CurrentLevelString = FString("Level: ");
        CurrentLevelString.Append(FString::FromInt(GameInstance->LevelCount));
        FText CurrentLevelText = FText::AsCultureInvariant(CurrentLevelString);
        LevelCounterTextBlock->SetText(CurrentLevelText);
    }   
}
