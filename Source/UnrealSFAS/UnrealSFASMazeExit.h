// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UnrealSFASMazeExit.generated.h"

UCLASS()
class UNREALSFAS_API AUnrealSFASMazeExit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASMazeExit();

    UPROPERTY(EditDefaultsOnly);
    UStaticMeshComponent* StaticMeshComponent;

    UPROPERTY(EditDefaultsOnly);
    UMaterial* ClosedMaterial;

    UPROPERTY(EditDefaultsOnly);
    UMaterial* OpenMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    UFUNCTION()
    virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
            UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    UPROPERTY(EditDefaultsOnly)
    float FadeTime;

public:	
    UFUNCTION()
    void OpenExit();

private:
    UFUNCTION()
    void NextLevel();

};
