// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UnrealSFASCharacter.generated.h"

DECLARE_MULTICAST_DELEGATE(FEnableSlowMo)

class USceneCaptureComponent2D;
class UUnrealSFASHealthComponent;
class UPaperSpriteComponent;
class AUnrealSFASBaseProjectile;
class AUnrealSFASGameState;

UCLASS(config=Game)
class AUnrealSFASCharacter : public APawn
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USceneCaptureComponent2D* MinimapSceneCaptureComponent2D;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UPaperSpriteComponent* PlayerMinimapSprite;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* StaticMeshComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* ShieldStaticMeshComponent;

    // Used as pivot point for the rotation of the cone
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USceneComponent* ConePivotPoint;

    // Used to show where the player is aiming
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* ConeStaticMesh;

public:
	AUnrealSFASCharacter();

    ///////////////////////////////////////////////////////////////
    // Status
    ///////////////////////////////////////////////////////////////

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Status")
    UUnrealSFASHealthComponent* HealthComponent;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Status")
    UMaterialInstance* DefaultMaterial;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Status")
    UMaterialInstance* DamagedMaterial;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Status")
    UMaterialInstance* SlowMoMaterial;

    ///////////////////////////////////////////////////////////////
    // PlayerControls
    ///////////////////////////////////////////////////////////////

    UPROPERTY(VisibleAnywhere, Category = "PlayerControls")
    FVector MouseDirection;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PlayerControls")
    bool bIsOrientingBurst;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PlayerControls")
    bool bCanBurst;

    UPROPERTY(VisibleAnywhere, Category = "PlayerControls")
    bool bLeftClickPressed;

    UPROPERTY(VisibleAnywhere, Category = "PlayerControls")
    bool bRightClickPressed;

    UPROPERTY(EditAnywhere, Category = "PlayerControls")
    float BurstMagnitudeCurve;

    UPROPERTY(EditAnywhere, Category = "PlayerControls")
    float BurstMagnitudeMax;

    UPROPERTY(EditAnywhere, Category = "PlayerControls")
    float BurstMagnitudeMin;

    UPROPERTY(VisibleAnywhere, Category = "PlayerControls")
    float BurstMagnitude;

    UPROPERTY(EditDefaultsOnly, Category = "PlayerControls")
    float AfterHitBurstCooldown;

    ///////////////////////////////////////////////////////////////
    // Weapon
    ///////////////////////////////////////////////////////////////

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TSubclassOf<AUnrealSFASBaseProjectile> Projectile;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
    float WeaponEnergyCost;

    UPROPERTY(EditAnywhere, Category = "Weapon")
    float MaxWeaponEnergy;

    UPROPERTY(VisibleAnywhere, Category = "Weapon")
    float CurrentWeaponEnergy;

    UPROPERTY(EditAnywhere, Category = "Weapon")
    float WeaponEnergyReloadAmount;

    UPROPERTY(VisibleAnywhere, Category = "Weapon")
    float WeaponCooldownTimer;

    UPROPERTY(EditAnywhere, Category = "Weapon")
    float MaxWeaponCooldownTimer;

    ///////////////////////////////////////////////////////////////
    // Shield
    ///////////////////////////////////////////////////////////////

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
    float ShieldEnergyCost;

    UPROPERTY(EditAnywhere, Category = "Shield")
    float MaxShieldEnergy;

    UPROPERTY(VisibleAnywhere, Category = "Shield")
    float CurrentShieldEnergy;

    UPROPERTY(EditAnywhere, Category = "Shield")
    float ShieldEnergyReloadAmount;

    UPROPERTY(VisibleAnywhere, Category = "Shield")
    float ShieldCooldownTimer;

    UPROPERTY(EditAnywhere, Category = "Shield")
    float MaxShieldCooldownTimer;

protected:
    void BeginPlay() override;

    void Tick(float DeltaSeconds) override;

    void Destroyed() override;

	/////** 
	//// * Called via input to turn at a given rate. 
	//// * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	//// */
	////void TurnAtRate(float Rate);

	/////**
	//// * Called via input to turn look up/down at a given rate. 
	//// * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	//// */
	////void LookUpAtRate(float Rate);

private:
    void GetProjectMouseDirectionAndLenghtToWorld(FVector& Direction, float& Lenght);

    void EnableSlowMo(const bool Enabled);
    DECLARE_DELEGATE_OneParam(FEnableSlowMoDelegate, const bool);

    void OrientBurst();

    UFUNCTION()
    void ExecuteBurst();

    UFUNCTION()
    void ShootProjectile();

    UFUNCTION()
    void ActivateShield();

    UFUNCTION()
    void WeaponRelease();

    UFUNCTION()
    void WeaponPressed();

    UFUNCTION()
    void ShieldRelease();

    UFUNCTION()
    void ShieldPressed();

    UFUNCTION()
    bool CanShoot();

    UFUNCTION()
    bool CanShield();

    UFUNCTION()
    void OpenMinimap();

    UFUNCTION()
    void CloseMinimap();

    UFUNCTION()
    void SetMeshMaterial(UMaterialInstance* MaterialInstance);

    UFUNCTION()
    void OnContact(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

    UPROPERTY()
    AUnrealSFASGameState* GameState;

    UPROPERTY(VisibleAnywhere, Category = "Status")
    float AdjustedDeltaSeconds;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

    UFUNCTION()
    void HealthModified();

    UFUNCTION()
    void SetCanBurst(const bool Value);

    UFUNCTION()
    void ToggleInputs(const bool Value);
};

