// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASGameState.h"

#include "Kismet/GameplayStatics.h"

#include "UnrealSFASCharacter.h"
#include "UnrealSFASHealthComponent.h"

AUnrealSFASGameState::AUnrealSFASGameState()
    : GlobalTimeDilationValue(0.125f)
{}

void AUnrealSFASGameState::EnablePause(const bool Value)
{
    const float NewGlobalTimeDilation = Value ? 0.0f : 1.0f;
    UGameplayStatics::SetGlobalTimeDilation(GetWorld(), NewGlobalTimeDilation);
}

void AUnrealSFASGameState::OnEnableActorsSlowMo(const bool Enabled)
{
    const float globalDilation = Enabled ? GlobalTimeDilationValue : 1.0f;
    UGameplayStatics::SetGlobalTimeDilation(GetWorld(), globalDilation);

    InvertedGlobalTimeDilationValue = Enabled ? (1.0f / GlobalTimeDilationValue) : 1.0f;

    OnEnableActorsSlowMo_Delegate.Broadcast(Enabled);
}
