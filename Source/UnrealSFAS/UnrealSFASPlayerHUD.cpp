// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealSFASPlayerHUD.h"
#include "Kismet/GameplayStatics.h"

#include "UnrealSFASPlayerWidget.h"

AUnrealSFASPlayerHUD::AUnrealSFASPlayerHUD()
{}

void AUnrealSFASPlayerHUD::DrawHUD()
{
    Super::DrawHUD();
}

void AUnrealSFASPlayerHUD::BeginPlay()
{
    Super::BeginPlay();

    if (PlayerWidgetClass)
    {
        APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

        if (PlayerController)
        {
            PlayerWidget = CreateWidget<UUnrealSFASPlayerWidget>(PlayerController, PlayerWidgetClass);

            if (PlayerWidget)
            {
                PlayerWidget->AddToViewport();
            }

            PlayerController->bShowMouseCursor = true;
        }
    }
}

void AUnrealSFASPlayerHUD::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}
