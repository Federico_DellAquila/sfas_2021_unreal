// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealSFASBaseEnemy.h"
#include "UnrealSFASBouncingEnemy.generated.h"

UCLASS()
class UNREALSFAS_API AUnrealSFASBouncingEnemy : public AUnrealSFASBaseEnemy
{
	GENERATED_BODY()

public:
    AUnrealSFASBouncingEnemy();

    UPROPERTY(VisibleAnywhere, Category = "Movement")
    FVector MovementDirection;

    UPROPERTY(EditAnywhere, Category = "Movement")
    float Speed;
    
    void OnContact(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

protected:
    void BeginPlay() override;
    void Tick(float DeltaSeconds) override;

private:
    void SelectNewMovementDirection();
};
