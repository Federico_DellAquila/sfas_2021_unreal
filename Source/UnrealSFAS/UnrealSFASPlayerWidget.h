// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UnrealSFASPlayerWidget.generated.h"

class UImage;
class UCanvasPanel;
class USizeBox;
class UScaleBox;
class UTextBlock;
class UVerticalBox;
class UProgressBar;
class UButton;
class AUnrealSFASCharacter;

UCLASS()
class UNREALSFAS_API UUnrealSFASPlayerWidget : public UUserWidget
{
	GENERATED_BODY()
public:
    UUnrealSFASPlayerWidget(const FObjectInitializer& ObjectInitializer);

    void NativeConstruct() override;

    ///////////////////////////////////////////////////////////////
    // Container
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UCanvasPanel* Canvas;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UCanvasPanel* GameplayUICanvasPanel;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UCanvasPanel* PauseMenuCanvasPanel;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* BarsSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* BarsScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UVerticalBox* BarsVerticalBox;

    ///////////////////////////////////////////////////////////////
    // Minimap
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* MinimapSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* MinimapScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UImage* MinimapImage;

    ///////////////////////////////////////////////////////////////
    // Health text
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* HealthSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* HealthScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UCanvasPanel* HealthProgressBarCanvasPanel;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* HealthProgressBarSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* HealthProgressBarScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UProgressBar* HealthProgressBar;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* HealthTextScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* HealthTextBlock;

    ///////////////////////////////////////////////////////////////
    // Level counter
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* LevelCounterTextSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* LevelCounterTextScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* LevelCounterTextBlock;

    ///////////////////////////////////////////////////////////////
    // Enemy counter
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* EnemyCounterTextSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* EnemyCounterTextScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* EnemyCounterTextBlock;

    ///////////////////////////////////////////////////////////////
    // Screen Effect
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* ScreenEffectSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* ScreenEffectScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UImage* ScreenEffectImage;

    ///////////////////////////////////////////////////////////////
    // Weapon bar
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* WeaponSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* WeaponScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UProgressBar* WeaponProgressBar;

    ///////////////////////////////////////////////////////////////
    // Shield bar
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* ShieldSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* ShieldScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UProgressBar* ShieldProgressBar;

    ///////////////////////////////////////////////////////////////
    // Pause Menu
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* PauseMenuSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* PauseMenuScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UVerticalBox* PauseMenuVerticalBox;

    ///////////////////////////////////////////////////////////////
    // You Died
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* YouDiedTextSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* YouDiedTextScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* YouDiedTextBlock;

    ///////////////////////////////////////////////////////////////
    // Main Menu
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* MainMenuButtonSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* MainMenuButtonScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UButton* MainMenuButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* MainMenuTextBlock;

    ///////////////////////////////////////////////////////////////
    // Main Menu
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* QuitButtonSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* QuitButtonScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UButton* QuitButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* QuitTextBlock;

    ///////////////////////////////////////////////////////////////
    // Resume button
    ///////////////////////////////////////////////////////////////
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    USizeBox* ResumeButtonSizeBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UScaleBox* ResumeButtonScaleBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UButton* ResumeButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
    UTextBlock* ResumeTextBlock;

    UFUNCTION()
    void SetDrawBurstLine(const bool Value);

    UFUNCTION()
    void SetScreenEffectColor(const FColor Color);

    UFUNCTION()
    void OpenMinimap();

    UFUNCTION()
    void CloseMinimap();

    UFUNCTION()
    void UpdateEnemiesNumber();

    UFUNCTION()
    void UpdateWeaponBar();

    UFUNCTION()
    void UpdateShieldBar();

    UFUNCTION()
    void UpdateHealthBar();

    UFUNCTION()
    void EnablePauseMenu(const bool Value, const bool bIsDead = false);

    UFUNCTION()
    void Quit();

    UFUNCTION()
    void Resume();

    UFUNCTION()
    void MainMenu();

    UFUNCTION()
    void SetLevelCounter();

protected:
    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    bool bDrawBurstLine;

    UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
    bool bIsPause;

    UPROPERTY()
    AUnrealSFASCharacter* PlayerRef;
};
