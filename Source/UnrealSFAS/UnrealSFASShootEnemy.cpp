#include "UnrealSFASShootEnemy.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "UObject/WeakObjectPtrTemplates.h"
#include "DrawDebugHelpers.h"

#include "UnrealSFASCharacter.h"
#include "UnrealSFASBaseProjectile.h"

AUnrealSFASShootEnemy::AUnrealSFASShootEnemy()
{
    DetectionRadius = 1500.0f;
    MaxCooldown = 2.5f;

    StaticMeshComponent->BodyInstance.bLockYTranslation = true;
    StaticMeshComponent->BodyInstance.bLockZTranslation = true;
}

void AUnrealSFASShootEnemy::BeginPlay()
{
    Super::BeginPlay();

    PlayerRef = Cast<AUnrealSFASCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

void AUnrealSFASShootEnemy::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (PlayerRef)
    {
        // If player is in range
        const float Distance = FVector::Dist(GetActorLocation(), PlayerRef->GetActorLocation());
        if (Distance <= DetectionRadius)
        {
            FHitResult OutHit;
            FVector Start = GetActorLocation();
            FVector End = PlayerRef->GetActorLocation();
            FCollisionQueryParams CollisionParams;
            CollisionParams.AddIgnoredActor(this);

            GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);

            // If the player is in sight
            if (OutHit.Actor == PlayerRef)
            {
                DrawDebugLine(GetWorld(), Start, End, FColor::Yellow, false, -1.0f, 0, 3.0f);

                // If the cooldown is complete
                if (CurrentCooldown <= MaxCooldown)
                {   
                    // Keep increasing the counter if the cooldown is not finished
                    CurrentCooldown += AdjustedDeltaSeconds;
                }
                else
                {   // Shoot
                    CurrentCooldown = 0;
                    ShootProjectile();
                }
            }
        }
    }
}

void AUnrealSFASShootEnemy::ShootProjectile()
{
    if (PlayerRef)
    {
        // Calculate the direction to the Player and spawn a projectile
        FVector Loc = GetActorLocation();
        FRotator Rot = UKismetMathLibrary::FindLookAtRotation(Loc, PlayerRef->GetActorLocation());
        FVector Scale = FVector(1.0f, 1.0f, 1.0f);
        FTransform Transf = FTransform(Rot, Loc, Scale);
        FActorSpawnParameters Parameters;

        GetWorld()->SpawnActor<AUnrealSFASBaseProjectile>(Projectile, Transf, Parameters);
    }
}
