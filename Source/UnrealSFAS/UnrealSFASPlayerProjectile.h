// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealSFASBaseProjectile.h"
#include "UnrealSFASPlayerProjectile.generated.h"

class UCapsuleComponent;

UCLASS()
class UNREALSFAS_API AUnrealSFASPlayerProjectile : public AUnrealSFASBaseProjectile
{
	GENERATED_BODY()

public:
    AUnrealSFASPlayerProjectile();

    UPROPERTY(EditDefaultsOnly, Category = "Collision")
    UCapsuleComponent* CapsuleCollider;

    virtual void BeginPlay() override;

    virtual void Tick(float DeltaSeconds) override;

    virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
            UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
};
