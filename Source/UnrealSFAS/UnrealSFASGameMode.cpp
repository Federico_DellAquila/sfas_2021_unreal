// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealSFASGameMode.h"

#include "Kismet/GameplayStatics.h"

//#include "UnrealSFASCharacter.h"
#include "UnrealSFASGameState.h"
#include "UnrealSFASPlayerHUD.h"
#include "UnrealSFASPlayerWidget.h"
#include "UnrealSFASMaze.h"

AUnrealSFASGameMode::AUnrealSFASGameMode()
{}

void AUnrealSFASGameMode::BeginPlay()
{
    StartingEnemiesNumber = GetEnemiesNumber();
}

void AUnrealSFASGameMode::CheckAmountEnemies()
{
    if (CurrentMaze) 
    {
        if (CurrentMaze->EnemiesList.Num() == 0)
        {
            CurrentMaze->OpenExit();

            AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
            if (HUD)
            {
                HUD->PlayerWidget->UpdateEnemiesNumber();
            }
        }
    }
}

void AUnrealSFASGameMode::RemoveEnemy(AUnrealSFASBaseEnemy* TargetEnemy)
{
    if (CurrentMaze)
    {
        CurrentMaze->EnemiesList.RemoveSingle(TargetEnemy);
    }

    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        HUD->PlayerWidget->UpdateEnemiesNumber();
    }

    CheckAmountEnemies();
}

uint32 AUnrealSFASGameMode::GetEnemiesNumber() const
{
    if (CurrentMaze)
    {
        return CurrentMaze->EnemiesList.Num();
    }

    return 0;
}

uint32 AUnrealSFASGameMode::GetStartingEnemiesNumber() const
{
    return StartingEnemiesNumber;
}