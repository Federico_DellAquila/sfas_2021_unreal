#include "UnrealSFASMainMenu.h"

// Engine Includes
#include "Runtime/UMG/Public/UMG.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "Kismet/GameplayStatics.h"

UUnrealSFASMainMenu::UUnrealSFASMainMenu(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    bIsFading = false;
    CurrentTimer = 0.0f;
    MaxTimer = 2.0f;
}

void UUnrealSFASMainMenu::NativeConstruct()
{
    Super::NativeConstruct();

    if (BlackScreen)
    {
        BlackScreen->SetOpacity(0.0f);
    }

    if (StartButton)
    {
        StartButton->OnPressed.AddDynamic(this, &UUnrealSFASMainMenu::StartGame);
    }

    if (QuitButton)
    {
        QuitButton->OnPressed.AddDynamic(this, &UUnrealSFASMainMenu::QuitGame);
    }
}

void UUnrealSFASMainMenu::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
    Super::NativeTick(MyGeometry, InDeltaTime);

    // Fade screen
    if (bIsFading)
    {
        if (CurrentTimer < MaxTimer)
        {
            CurrentTimer += InDeltaTime;

            // Load new Level after a certain amount of tme
            if (CurrentTimer >= MaxTimer)
            {
                OpenNewLevel();
            }
        }

        // Since ScreenFade doesn't cover Widgets, I'm fading a black image on top of everything
        float LinearValue = CurrentTimer / MaxTimer;

        if (BlackScreen)
        {
            BlackScreen->SetOpacity(LinearValue);
        }
    }
}

void UUnrealSFASMainMenu::StartGame()
{
    FTimerHandle Timer;
    UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetWorldTimerManager().SetTimer(Timer, this, &UUnrealSFASMainMenu::OpenNewLevel, 2.5f, false);
    bIsFading = true;
}

void UUnrealSFASMainMenu::QuitGame()
{
    FGenericPlatformMisc::RequestExit(false);
}

void UUnrealSFASMainMenu::OpenNewLevel()
{
    // Reload the level
    UGameplayStatics::OpenLevel(GetWorld(), "MazeBase");
}
