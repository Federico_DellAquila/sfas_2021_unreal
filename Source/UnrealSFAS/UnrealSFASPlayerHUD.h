// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "UnrealSFASPlayerHUD.generated.h"

class UUnrealSFASPlayerWidget;

UCLASS()
class UNREALSFAS_API AUnrealSFASPlayerHUD : public AHUD
{
	GENERATED_BODY()
	
public:
    AUnrealSFASPlayerHUD();

    virtual void DrawHUD() override;

    virtual void BeginPlay() override;

    virtual void Tick(float DeltaSeconds) override;

    UPROPERTY(EditDefaultsOnly, Category = "Widgets")
    TSubclassOf<UUserWidget> PlayerWidgetClass;

    UUnrealSFASPlayerWidget* PlayerWidget;
};
