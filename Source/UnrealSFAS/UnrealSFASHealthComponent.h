// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UnrealSFASHealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNREALSFAS_API UUnrealSFASHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUnrealSFASHealthComponent();

    UPROPERTY(EditDefaultsOnly, Category = "Health");
    UMaterial* DamagedMaterial;

    int GetHealthValue() const;
    void SetHealthValue(const int Value);
    int GetMaxHealthValue() const;
    void SetCanBeDamaged(const bool Value);
    bool GetCanBeDamaged() const;
    void ApplyDamage(const int Value);
    void RecoverHealth(const int Value);
    void Death();

protected:
    UPROPERTY(VisibleAnywhere, Category = "Health", meta = (ClampMin = "0", ClampMax = "100"))
    int HealthValue;

    UPROPERTY(EditAnywhere, Category = "Health", meta = (ClampMin = "0", ClampMax = "100"))
    int MaxHealthValue;

    UPROPERTY(VisibleAnywhere, Category = "Health")
    bool bCanBeDamaged;
};
