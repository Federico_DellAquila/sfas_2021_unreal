// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealSFASHealthComponent.h"

// Sets default values for this component's properties
UUnrealSFASHealthComponent::UUnrealSFASHealthComponent()
    : HealthValue(100)
    , MaxHealthValue(100)
    , bCanBeDamaged(true)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


int UUnrealSFASHealthComponent::GetHealthValue() const
{
    return HealthValue;
}

void UUnrealSFASHealthComponent::SetHealthValue(const int Value)
{
    HealthValue = Value;
    HealthValue = FMath::Clamp(HealthValue, 0, MaxHealthValue);
}

int UUnrealSFASHealthComponent::GetMaxHealthValue() const
{
    return MaxHealthValue;
}

void UUnrealSFASHealthComponent::SetCanBeDamaged(const bool Value)
{
    bCanBeDamaged = Value;
}

bool UUnrealSFASHealthComponent::GetCanBeDamaged() const
{
    return bCanBeDamaged;
}

void UUnrealSFASHealthComponent::ApplyDamage(const int Value)
{
    HealthValue -= FMath::Abs(Value);
    HealthValue = FMath::Clamp(HealthValue, 0, 100);

    if (HealthValue == 0)
    {
        Death();
    }
}

void UUnrealSFASHealthComponent::RecoverHealth(const int Value)
{
    HealthValue += FMath::Abs(Value);
    HealthValue = FMath::Clamp(HealthValue, 0, 100);
}

void UUnrealSFASHealthComponent::Death()
{
    GetOwner()->Destroy();
}

