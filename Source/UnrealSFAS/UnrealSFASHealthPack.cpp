#include "UnrealSFASHealthPack.h"

#include "Components/SphereComponent.h"

#include "UnrealSFASDamagerComponent.h"

// Sets default values
AUnrealSFASHealthPack::AUnrealSFASHealthPack()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    SphereCollider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollider"));
    SphereCollider->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    SphereCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
    SphereCollider->SetGenerateOverlapEvents(true);
    SphereCollider->SetSimulatePhysics(false);
    SphereCollider->SetEnableGravity(false);
    SetRootComponent(SphereCollider);


    CrossPieceVertical = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CrossPieceVertical"));
    CrossPieceVertical->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    CrossPieceVertical->SetSimulatePhysics(false);
    CrossPieceVertical->SetEnableGravity(false);
    CrossPieceVertical->SetupAttachment(RootComponent);


    CrossPieceHorizontal = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CrossPieceHorizontal"));
    CrossPieceHorizontal->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    CrossPieceHorizontal->SetSimulatePhysics(false);
    CrossPieceHorizontal->SetEnableGravity(false);
    CrossPieceHorizontal->SetupAttachment(RootComponent);

    DamagerComponent = CreateDefaultSubobject<UUnrealSFASDamagerComponent>(TEXT("DamagerComponent"));
    DamagerComponent->SetDamageValue(20);
    DamagerComponent->bIsDamage = false;
}