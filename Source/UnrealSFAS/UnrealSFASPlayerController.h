// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UnrealSFASPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSFAS_API AUnrealSFASPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
    AUnrealSFASPlayerController();

    virtual void BeginPlay() override;

    virtual void SetupInputComponent() override;

    UFUNCTION()
    void TogglePause();

    bool bIsPause;
	
};
