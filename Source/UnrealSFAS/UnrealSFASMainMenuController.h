// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UnrealSFASMainMenuController.generated.h"

class UUnrealSFASMainMenu;

UCLASS()
class UNREALSFAS_API AUnrealSFASMainMenuController : public APlayerController
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AUnrealSFASMainMenuController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<UUnrealSFASMainMenu> MainMenuClass;
};
