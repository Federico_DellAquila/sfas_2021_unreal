// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealSFASGameMode.generated.h"

class AUnrealSFASMaze;
class AUnrealSFASBaseEnemy;

UCLASS(minimalapi)
class AUnrealSFASGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnrealSFASGameMode();

    virtual void BeginPlay() override;

    UPROPERTY(VisibleAnywhere, Category = "Level")
    AUnrealSFASMaze* CurrentMaze;

    UFUNCTION()
    void CheckAmountEnemies();

    UFUNCTION()
    void RemoveEnemy(AUnrealSFASBaseEnemy* TargetEnemy);

    UFUNCTION()
    uint32 GetEnemiesNumber() const;

    UFUNCTION()
    uint32 GetStartingEnemiesNumber() const;

private:
    uint32 StartingEnemiesNumber;
};



