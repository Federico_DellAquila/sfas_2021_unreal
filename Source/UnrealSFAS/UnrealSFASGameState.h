// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "UnrealSFASGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnableActorsSlowMoDelegate, bool, Enabled);

class AUnrealSFASCharacter;

UCLASS()
class UNREALSFAS_API AUnrealSFASGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
    AUnrealSFASGameState();

    UFUNCTION()
    void EnablePause(const bool Value);

    // Used to notify an interest actors that SlowMo has been enabled
    UFUNCTION()
    void OnEnableActorsSlowMo(const bool Enabled);
    FOnEnableActorsSlowMoDelegate OnEnableActorsSlowMo_Delegate;

    // This is used to counter the Global Time Dilation by multiplying it to DeltaTime
    UPROPERTY(EditAnywhere, Category = "Time", meta = (ClampMin = "0.0", ClampMax = "1.0"))
    float InvertedGlobalTimeDilationValue;

private:
    UPROPERTY(EditAnywhere, Category = "Time", meta = (ClampMin = "0.0", ClampMax = "1.0"))
    float GlobalTimeDilationValue;

    UPROPERTY()
    AUnrealSFASCharacter* PlayerRef;
};
