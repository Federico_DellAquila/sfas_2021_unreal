// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "UnrealSFASISlowMo.h"

#include "UnrealSFASBaseEnemy.generated.h"

class UUnrealSFASHealthComponent;
class UUnrealSFASDamagerComponent;
class AUnrealSFASGameState;
class AUnrealSFASHealthPack;
class AUnrealSFASCharacter;

UCLASS()
class UNREALSFAS_API AUnrealSFASBaseEnemy : public AActor, public IUnrealSFASISlowMo
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASBaseEnemy();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
    UStaticMeshComponent* StaticMeshComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
    UUnrealSFASHealthComponent* HealthComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
    UUnrealSFASDamagerComponent* DamagerComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop")
    TSubclassOf<AUnrealSFASHealthPack> HealthPackClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop")
    int HealthPackDropChance;

protected:
    UPROPERTY()
    AUnrealSFASGameState* GameState;

    UPROPERTY()
    AUnrealSFASCharacter* PlayerRef;

    UPROPERTY(VisibleAnywhere, Category = "Details")
    float AdjustedDeltaSeconds;

    virtual void BeginPlay() override;

    virtual void Tick(float DeltaSeconds) override;

    virtual void Destroyed() override;

public:	
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SlowMoFunction")
    void EnableSlowMo(const bool Enabled);
    virtual void EnableSlowMo_Implementation(const bool Enabled) override;

    UFUNCTION()
    virtual void OnContact(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};