// Copyright Epic Games, Inc. All Rights Reserved.

// Engine includes
#include "UnrealSFASCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "PaperSpriteComponent.h" // Paper2D

// Project Includes
#include "UnrealSFASGameState.h"
#include "UnrealSFASHealthComponent.h"
#include "UnrealSFASDamagerComponent.h"
#include "UnrealSFASGameMode.h"
#include "UnrealSFASMaze.h"
#include "UnrealSFASPlayerHUD.h"
#include "UnrealSFASPlayerWidget.h"
#include "UnrealSFASBaseProjectile.h"
#include "UnrealSFASPlayerProjectile.h"
#include "UnrealSFASGameState.h"
#include "UnrealSFASGameInstance.h"

//////////////////////////////////////////////////////////////////////////
// AUnrealSFASCharacter

AUnrealSFASCharacter::AUnrealSFASCharacter()
{
    HealthComponent = CreateDefaultSubobject<UUnrealSFASHealthComponent>(TEXT("HealthComponent"));

    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

    StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
    StaticMeshComponent->SetWorldScale3D(FVector(0.125f, 0.125f, 0.125f));
    StaticMeshComponent->SetGenerateOverlapEvents(true);
    StaticMeshComponent->SetSimulatePhysics(true);
    StaticMeshComponent->SetEnableGravity(true);
    StaticMeshComponent->SetNotifyRigidBodyCollision(true);
    StaticMeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
    StaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
    StaticMeshComponent->SetUseCCD(true);
    StaticMeshComponent->BodyInstance.bLockZRotation = true;
    StaticMeshComponent->BodyInstance.bLockXRotation = true;
    StaticMeshComponent->BodyInstance.bLockYRotation = true;
    StaticMeshComponent->BodyInstance.bLockXTranslation = true;
    SetRootComponent(StaticMeshComponent);

    ShieldStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShieldStaticMeshComponent"));
    ShieldStaticMeshComponent->SetWorldScale3D(FVector(7.5f));
    ShieldStaticMeshComponent->SetSimulatePhysics(false);
    ShieldStaticMeshComponent->SetEnableGravity(false);
    ShieldStaticMeshComponent->SetGenerateOverlapEvents(false);
    ShieldStaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    ShieldStaticMeshComponent->SetVisibility(false);
    ShieldStaticMeshComponent->SetupAttachment(RootComponent);

    ConePivotPoint = CreateDefaultSubobject<USceneComponent>(TEXT("ConePivotPoint"));
    ConePivotPoint->SetupAttachment(RootComponent);

    ConeStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ConeStaticMesh"));
    ConeStaticMesh->SetSimulatePhysics(false);
    ConeStaticMesh->SetEnableGravity(false);
    ConeStaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    ConeStaticMesh->SetupAttachment(ConePivotPoint);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 1375.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = false; // Do not rotate the arm based on the controller
    CameraBoom->bInheritPitch = false;
    CameraBoom->bInheritRoll = false;
    CameraBoom->bInheritYaw = false;
    CameraBoom->bDoCollisionTest = false;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

    MinimapSceneCaptureComponent2D = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("MinimapCamera"));
    MinimapSceneCaptureComponent2D->SetupAttachment(FollowCamera);
    MinimapSceneCaptureComponent2D->AddWorldOffset(FVector(-500.0f, 0.0f, 0.0f));
    MinimapSceneCaptureComponent2D->ProjectionType = ECameraProjectionMode::Orthographic;
    MinimapSceneCaptureComponent2D->OrthoWidth = 3325.0f;

    PlayerMinimapSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("PlayerMinimapSprite"));
    PlayerMinimapSprite->SetupAttachment(FollowCamera);
    PlayerMinimapSprite->SetWorldScale3D(FVector(10.0f, 10.0f, 10.0f));
    PlayerMinimapSprite->AddWorldRotation(FRotator(0.0f, -90, 0.0f));
    PlayerMinimapSprite->SetEnableGravity(false);
    PlayerMinimapSprite->bApplyImpulseOnDamage = false;
    PlayerMinimapSprite->bReplicatePhysicsToAutonomousProxy = false;
    PlayerMinimapSprite->SetGenerateOverlapEvents(false);
    PlayerMinimapSprite->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    PlayerMinimapSprite->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AUnrealSFASCharacter::OnContact);

    // Player movement settings
    BurstMagnitudeCurve = 13.0f;
    BurstMagnitudeMin = 350.0f;
    BurstMagnitudeMax = 1500.0f;
    SetCanBurst(true);
    AfterHitBurstCooldown = 0.33f;

    // Weapon
    WeaponEnergyCost = 1.0f;
    MaxWeaponEnergy = 20.0f;
    CurrentWeaponEnergy = MaxWeaponEnergy;
    WeaponEnergyReloadAmount = 10.0f;
    WeaponCooldownTimer = MaxWeaponCooldownTimer;
    MaxWeaponCooldownTimer = 0.08f;

    // Shield
    ShieldEnergyCost = 1.0f;
    MaxShieldEnergy = 1.0f;
    CurrentShieldEnergy = MaxShieldEnergy;
    ShieldEnergyReloadAmount = 0.5f;
    ShieldCooldownTimer = MaxShieldCooldownTimer;
    MaxShieldCooldownTimer = 0.0f;
}

//////////////////////////////////////////////////////////////////////////
// Input
//////////////////////////////////////////////////////////////////////////
void AUnrealSFASCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

    PlayerInputComponent->BindAction<FEnableSlowMoDelegate>("SpaceBarButton", IE_Pressed, this, &AUnrealSFASCharacter::EnableSlowMo, true);
    PlayerInputComponent->BindAction("SpaceBarButton", IE_Released, this, &AUnrealSFASCharacter::ExecuteBurst);
    PlayerInputComponent->BindAction<FEnableSlowMoDelegate>("SpaceBarButton", IE_Released, this, &AUnrealSFASCharacter::EnableSlowMo, false);

    PlayerInputComponent->BindAction("LeftClick", IE_Pressed, this, &AUnrealSFASCharacter::WeaponPressed);
    PlayerInputComponent->BindAction("LeftClick", IE_Released, this, &AUnrealSFASCharacter::WeaponRelease);

    PlayerInputComponent->BindAction("RightClick", IE_Pressed, this, &AUnrealSFASCharacter::ShieldPressed);
    PlayerInputComponent->BindAction("RightClick", IE_Released, this, &AUnrealSFASCharacter::ShieldRelease);

    PlayerInputComponent->BindAction("Minimap", IE_Pressed, this, &AUnrealSFASCharacter::OpenMinimap);
    PlayerInputComponent->BindAction("Minimap", IE_Released, this, &AUnrealSFASCharacter::CloseMinimap);
}

void AUnrealSFASCharacter::BeginPlay()
{
    Super::BeginPlay();

    // Start at the Maze Start Location
    AUnrealSFASGameMode* GameMode = Cast<AUnrealSFASGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    if (GameMode)
    {
        SetActorLocation(GameMode->CurrentMaze->StartLocation);
    }

    // Store a reference to Game State
    GameState = Cast<AUnrealSFASGameState>(UGameplayStatics::GetGameState(AActor::GetWorld()));

    CurrentWeaponEnergy = MaxWeaponEnergy;
    CurrentShieldEnergy = MaxShieldEnergy;

    // Set the Health accordingly to the value stored in the Game Instance
    UUnrealSFASGameInstance* GameInstance = Cast<UUnrealSFASGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
    if (GameInstance)
    {
        const int LastLevelHealth = GameInstance->LastLevelHealth;

        if (LastLevelHealth != 0)
        {
            HealthComponent->SetHealthValue(GameInstance->LastLevelHealth);
        }
        else
        {
            HealthComponent->SetHealthValue(100);
        }
    }
}

void AUnrealSFASCharacter::Tick(float DeltaSeconds)
{
    // Adjust Delta seconds
    if (GameState)
    {
        AdjustedDeltaSeconds = DeltaSeconds * GameState->InvertedGlobalTimeDilationValue;
    }

    Super::Tick(AdjustedDeltaSeconds);

    // Find mouse position in the world to orient Burst and Shoot
    GetProjectMouseDirectionAndLenghtToWorld(MouseDirection, BurstMagnitude);
    ConePivotPoint->SetWorldRotation(MouseDirection.Rotation());

    // Calculate the Burst amount
    if (bIsOrientingBurst)
    {
        const float Curve = FMath::Loge(BurstMagnitudeCurve);
        BurstMagnitude = FMath::Clamp(BurstMagnitude * Curve, BurstMagnitudeMin, BurstMagnitudeMax);
    }

    // Reload weapon
    if (bLeftClickPressed == false)
    {
        if (CurrentWeaponEnergy < MaxWeaponEnergy)
        {
            CurrentWeaponEnergy += WeaponEnergyReloadAmount * AdjustedDeltaSeconds;
            CurrentWeaponEnergy = FMath::Clamp(CurrentWeaponEnergy, 0.0f, MaxWeaponEnergy);
        }
    }

    // Weapon cooldown
    if (WeaponCooldownTimer < MaxWeaponCooldownTimer)
    {
        WeaponCooldownTimer += AdjustedDeltaSeconds;
        WeaponCooldownTimer = FMath::Clamp(WeaponCooldownTimer, 0.0f, MaxWeaponCooldownTimer);
    }
    else
    {
        WeaponCooldownTimer = MaxWeaponCooldownTimer;
    }

    // Shooting
    if (CanShoot() &&                               // Cooldown is ready
        CurrentWeaponEnergy >= WeaponEnergyCost &&  // Enough energy
        bLeftClickPressed)                          // Left click pressed
    {
        ShootProjectile();
    }

    // Reload shield
    if (bRightClickPressed == false)
    {
        if (CurrentShieldEnergy < MaxShieldEnergy)
        {
            CurrentShieldEnergy += ShieldEnergyReloadAmount * AdjustedDeltaSeconds;
            CurrentShieldEnergy = FMath::Clamp(CurrentShieldEnergy, 0.0f, MaxShieldEnergy);
        }
    }

    // Shielding
    if (CanShield() &&               // Cooldown is ready
        bRightClickPressed)          // Left click pressed
    {
        ActivateShield();
    }

    // Keep Weapon and Shield energy bar Updated
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (HUD->PlayerWidget)
        {
            HUD->PlayerWidget->UpdateWeaponBar();
            HUD->PlayerWidget->UpdateShieldBar();
        }
    }
}

void AUnrealSFASCharacter::Destroyed()
{
    Super::Destroyed();

    // If the Player gets killed, open the menu
    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    if (PlayerController)
    {
        AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(PlayerController->GetHUD());
        if (HUD)
        {
            if (HUD->PlayerWidget)
            {
                HUD->PlayerWidget->EnablePauseMenu(true, true);
            }
        }
    }
}

void AUnrealSFASCharacter::GetProjectMouseDirectionAndLenghtToWorld(FVector& Direction, float& Lenght)
{
    // Deproject the mouse from screen to world
    FVector worldLocation = FVector();
    FVector worldDirection = FVector();
    APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    playerController->DeprojectMousePositionToWorld(worldLocation, worldDirection);

    // Find an intersectin with a plane at Player height
    FVector intersectionPoint = FMath::LinePlaneIntersection(worldLocation, worldLocation + (worldDirection * 3000.0f), GetActorLocation(), -FVector::ForwardVector);

    // Store Direction and Lenght of the intersection result
    Direction = (intersectionPoint - GetActorLocation()).GetSafeNormal();
    Lenght = FVector::Dist(GetActorLocation(), intersectionPoint);
}

void AUnrealSFASCharacter::EnableSlowMo(const bool Enabled)
{
    // When the Player enable slow mo, inform the GameState about this 
    AUnrealSFASGameState* gameState = Cast<AUnrealSFASGameState>(UGameplayStatics::GetGameState(GetWorld()));
    gameState->OnEnableActorsSlowMo(Enabled);
    bIsOrientingBurst = Enabled;
    SetCanBurst(Enabled);

    // Inform the UI to display the line and the screen effect
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (HUD->PlayerWidget)
        {
            HUD->PlayerWidget->SetDrawBurstLine(Enabled);
        }
    }

    // Change Player Material
    UMaterialInstance* MaterialInstance = Enabled ? SlowMoMaterial : DefaultMaterial;
    SetMeshMaterial(MaterialInstance);
}

void AUnrealSFASCharacter::HealthModified()
{
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (HUD->PlayerWidget)
        {
            HUD->PlayerWidget->UpdateHealthBar();
        }
    }
}

void AUnrealSFASCharacter::SetCanBurst(const bool Value)
{
    bCanBurst = Value;
}

void AUnrealSFASCharacter::ToggleInputs(const bool Value)
{
    // Disable or enable inputs
    if (Value)
    {
        EnableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));
    }
    else
    {
        DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));
        WeaponRelease();
        ShieldRelease();
    }

    // Add a screen effect that shows that inputs are disabled
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (HUD->PlayerWidget)
        {
            HUD->PlayerWidget->SetDrawBurstLine(Value);

            FColor TempColor = Value ? FColor::Transparent : FColor::Red;
            HUD->PlayerWidget->SetScreenEffectColor(TempColor);
        }
    }

    // Change Player Material
    UMaterialInstance* TempMaterialInstnce = Value ? DefaultMaterial : DamagedMaterial;
    SetMeshMaterial(TempMaterialInstnce);

    // Disable or enable gravity (better result when pushing back)
    StaticMeshComponent->SetEnableGravity(Value);
}

void AUnrealSFASCharacter::ExecuteBurst()
{
    if (bCanBurst)
    {
        StaticMeshComponent->SetAllPhysicsLinearVelocity(FVector(0.0f));
        StaticMeshComponent->AddImpulse(-MouseDirection * BurstMagnitude, NAME_None, true);
    }
}

void AUnrealSFASCharacter::ShootProjectile()
{
    if (CanShoot() && CurrentWeaponEnergy >= WeaponEnergyCost)
    {
        GameState = Cast<AUnrealSFASGameState>(UGameplayStatics::GetGameState(AActor::GetWorld()));
        if (GameState->IsValidLowLevel())
        {
            // Adjust the location of the spawn with the Player velocity
            FVector Velocity = StaticMeshComponent->GetPhysicsLinearVelocity() * UGameplayStatics::GetWorldDeltaSeconds(GetWorld()) * GameState->InvertedGlobalTimeDilationValue;
            FVector Loc = GetActorLocation() + Velocity;
            // Find the direction
            FRotator Rot = UKismetMathLibrary::FindLookAtRotation(Loc, Loc + MouseDirection);
            FActorSpawnParameters Parameters;
            Parameters.Owner = this;
            // Spawn projectile
            if (Projectile)
            {
                GetWorld()->SpawnActor<AUnrealSFASBaseProjectile>(Projectile, Loc, Rot * -1.0f, Parameters);
            }
            CurrentWeaponEnergy -= WeaponEnergyCost;
            WeaponCooldownTimer = 0;
        }
    }
}

void AUnrealSFASCharacter::ActivateShield()
{
    if (CanShield())
    {
        CurrentShieldEnergy -= ShieldEnergyCost * AdjustedDeltaSeconds;

        if (CurrentShieldEnergy <= 0)
        {
            ShieldRelease();
        }
        else
        {
            ShieldStaticMeshComponent->SetVisibility(true);
        }

        CurrentShieldEnergy = FMath::Clamp(CurrentShieldEnergy, 0.0f, MaxShieldEnergy);

        HealthComponent->SetCanBeDamaged(false);
    }
}

void AUnrealSFASCharacter::WeaponRelease()
{
    bLeftClickPressed = false;
}

void AUnrealSFASCharacter::WeaponPressed()
{
    bLeftClickPressed = true;
}

void AUnrealSFASCharacter::ShieldRelease()
{
    bRightClickPressed = false;
    HealthComponent->SetCanBeDamaged(true);
    ShieldStaticMeshComponent->SetVisibility(false);
}

void AUnrealSFASCharacter::ShieldPressed()
{
    bRightClickPressed = true;
}

bool AUnrealSFASCharacter::CanShoot()
{
    return WeaponCooldownTimer == MaxWeaponCooldownTimer;
}

bool AUnrealSFASCharacter::CanShield()
{
    return CurrentShieldEnergy >= 0.0f;
}

void AUnrealSFASCharacter::OpenMinimap()
{
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (HUD->PlayerWidget)
        {
            HUD->PlayerWidget->OpenMinimap();
            MinimapSceneCaptureComponent2D->OrthoWidth *= 4.0f;
        }
    }
}

void AUnrealSFASCharacter::CloseMinimap()
{
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (HUD->PlayerWidget)
        {
            HUD->PlayerWidget->CloseMinimap();
            MinimapSceneCaptureComponent2D->OrthoWidth *= 0.25f;
        }
    }
}

void AUnrealSFASCharacter::SetMeshMaterial(UMaterialInstance* MaterialInstance)
{
    if (MaterialInstance)
    {
        StaticMeshComponent->SetMaterial(0, MaterialInstance);
    }
}

void AUnrealSFASCharacter::OnContact(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (HealthComponent->GetCanBeDamaged() == true)
    {
        UUnrealSFASDamagerComponent* OtherDamagerComponent = Cast<UUnrealSFASDamagerComponent>(OtherActor->GetComponentByClass(UUnrealSFASDamagerComponent::StaticClass()));
        if (OtherDamagerComponent)
        {
            // Is it should damage
            if (OtherDamagerComponent->bIsDamage)
            {
                const FVector PushbackDirection = (GetActorLocation() - OtherActor->GetActorLocation()).GetSafeNormal();
                OtherDamagerComponent->AddPushback(StaticMeshComponent, PushbackDirection, 800.0f);

                EnableSlowMo(false);
                ToggleInputs(false);

                // Us a timer to re-enable inputs (work because Global Time Dilation is always 1 when this happen)
                FTimerHandle Timer;
                FTimerDelegate TimerDelToggleInputs;
                TimerDelToggleInputs.BindUFunction(this, FName("ToggleInputs"), true);
                GetWorldTimerManager().SetTimer(Timer, TimerDelToggleInputs, AfterHitBurstCooldown, false);

                HealthComponent->ApplyDamage(OtherDamagerComponent->GetDamageValue());
                HealthModified();
            }
            else // If it should heal (Health Pack)
            {
                HealthComponent->RecoverHealth(OtherDamagerComponent->GetDamageValue());
                HealthModified();

                OtherActor->Destroy();
            }
        }
    }
}
