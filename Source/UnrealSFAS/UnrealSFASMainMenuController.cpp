#include "UnrealSFASMainMenuController.h"

#include "UnrealSFASMainMenu.h"

// Sets default values
AUnrealSFASMainMenuController::AUnrealSFASMainMenuController()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AUnrealSFASMainMenuController::BeginPlay()
{
	Super::BeginPlay();
	
    // Used only for the main menu. Open the proper Widget.
    if (MainMenuClass)
    {
        UUnrealSFASMainMenu* MainMenuRef = CreateWidget<UUnrealSFASMainMenu>(this, MainMenuClass);
        if (MainMenuRef)
        {
            MainMenuRef->AddToViewport();
        }

        // Set mode and lock the mouse to the screen
        FInputModeGameAndUI InputMode;
        InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);
        InputMode.SetHideCursorDuringCapture(false);
        SetInputMode(InputMode);
        bShowMouseCursor = true;
    }
}

