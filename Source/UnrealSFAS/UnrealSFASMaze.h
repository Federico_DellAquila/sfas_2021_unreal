// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <random>
#include "UnrealSFASMaze.generated.h"

UENUM()
enum class EDirection
{
    DOWN = 0    UMETA(DisplayName = "DOWN"),
    LEFT = 1    UMETA(DisplayName = "LEFT"),
    UP = 2      UMETA(DisplayName = "UP"),
    RIGHT = 3   UMETA(DisplayName = "RIGHT")
};

// Random walker used to generate the maze procedurally
USTRUCT()
struct FRandomWalker
{
    GENERATED_USTRUCT_BODY();

    FRandomWalker() {};

    FRandomWalker(const uint32 NewBoundaryX, const uint32 NewBoundaryY)
    {
        boundaryX = NewBoundaryX - 2;
        boundaryY = NewBoundaryY - 2;

        positionX = FMath::RoundToInt(static_cast<float>(NewBoundaryX) / 2.0f);
        positionY = FMath::RoundToInt(static_cast<float>(NewBoundaryY) / 2.0f);
    };

    uint32 positionX;
    uint32 positionY;

    uint32 boundaryX;
    uint32 boundaryY;

    void Move(EDirection NewDirection)
    {
        uint32 newPos;

        switch (NewDirection)
        {
            case EDirection::UP:
                newPos = positionY + 1;
                newPos = FMath::Clamp(newPos, static_cast<uint32>(1), boundaryY);
                positionY = newPos;
                break;
            case EDirection::DOWN:
                newPos = positionY - 1;
                newPos = FMath::Clamp(newPos, static_cast <uint32>(1), boundaryY);
                positionY = newPos;
                break;
            case EDirection::RIGHT:
                newPos = positionX + 1;
                newPos = FMath::Clamp(newPos, static_cast <uint32>(1), boundaryX);
                positionX = newPos;
                break;
            case EDirection::LEFT:
                newPos = positionX - 1;
                newPos = FMath::Clamp(newPos, static_cast <uint32>(1), boundaryX);
                positionX = newPos;
                break;
        }
    }

    void SetPosition(uint32 X, uint32 Y)
    {
        positionX = FMath::Clamp(X, static_cast <uint32>(1), boundaryX);
        positionY = FMath::Clamp(Y, static_cast <uint32>(1), boundaryY);
    };
};

class AUnrealSFASBaseEnemy;
class AUnrealSFASMazeExit;

UCLASS()
class UNREALSFAS_API AUnrealSFASMaze : public AActor
{
	GENERATED_BODY()

private:
    // Return a string representing the binary value of an uint64
	FString Uint64ToBinary(uint64 value);

    void GenerateMazeArray();

    void GenerateSeed();

    void ClearMaze();

    void SpawnEnemies();
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASMaze();

	UFUNCTION(CallInEditor, Category = "MazeGeneration")
	void GenerateLevel();

    // Return 0 if the cell is empty or 1 if the cell is a wall
    bool GetCellValue(const uint32 Height, const uint32 Width) const;

    // Return true if the cells inside a square area are empty
    bool CheckIfAreaEmpty(const uint32 PosZ, const uint32 PosY, const uint32 LayersNum) const;

    // Set a cell value to 0 for empty and 1 for wall
    void SetCellValue(const uint32 Height, const uint32 Width, const bool Value);

    // Calculate the position in world space of the cell
    FVector GetCellPosition(const uint32 Height, const uint32 Width) const;

    void OpenExit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UPROPERTY(EditAnywhere, Category = "BlockDetails")
	UStaticMesh* WallMesh;

	UPROPERTY(EditAnywhere, Category = "BlockDetails")
	float blockSize = 200.0f;
	UPROPERTY(EditAnywhere, Category = "BlockDetails")
	float blockWidth = 2.0f;
	UPROPERTY(EditAnywhere, Category = "BlockDetails")
	float blockHeight = 5.0f;
	UPROPERTY(EditAnywhere, Category = "BlockDetails")
	float blockZPos = 50.0f;

    // Since we are dealing with values that can go up to 64 max, we need to limit the size of the map to 63
	UPROPERTY(EditAnywhere, Category = "MazeGeneration", meta = (ClampMin = "16", ClampMax = "63", UIMin = "16", UIMax = "63"))
	uint32 mazeWidth = 32;
    UPROPERTY(EditAnywhere, Category = "MazeGeneration", meta = (ClampMin = "16", ClampMax = "63", UIMin = "16", UIMax = "63"))
    uint32 mazeHeight = 32;
	UPROPERTY(EditAnywhere, Category = "MazeGeneration")
	bool bGenerateRandomSeed = true;

    // Seed used for procedural generation
	UPROPERTY(EditAnywhere, Category = "MazeGeneration", meta = (EditCondition = "!bGenerateRandomSeed"))
	uint32 levelSeed = 0;

    UPROPERTY(EditAnywhere, Category = "MazeGeneration")
    AUnrealSFASMazeExit* MazeExit;

    // Used to store a reference of every wall created
	UPROPERTY(EditAnywhere, Category = "MazeGeneration")
	TArray<UStaticMeshComponent*> staticMeshList;

	// Visual representation of the maze
	UPROPERTY(VisibleAnywhere, Category = "MazeGeneration")
	TArray<FString> mazeArrayVisual;

    // Array used to store the binary values of the maze rows
	TArray<uint64> mazeArray;

    UPROPERTY(EditAnywhere, Category = "Enemies")
    TArray<TSubclassOf<AUnrealSFASBaseEnemy>> EnemyTypes;

    UPROPERTY(EditAnywhere, Category = "Enemies")
    float EnemyPercentage;

    UPROPERTY(VisibleAnywhere, Category = "MazeGeneration")
    uint32 TotalEmptySpaces;

    // Player Start location
    UPROPERTY(VisibleAnywhere, Category = "MazeGeneration")
    FVector StartLocation;

    // Maze Exit location
    UPROPERTY(VisibleAnywhere, Category = "MazeGeneration")
    FVector EndLocation;

    // List of reference to each enemy in the maze
    UPROPERTY(VisibleAnywhere, Category = "Enemies")
    TArray<AUnrealSFASBaseEnemy*> EnemiesList;

    // Used for procedural generation
    std::mt19937 pseudoRandomGenerator;
};
