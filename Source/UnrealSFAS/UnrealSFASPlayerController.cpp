#include "UnrealSFASPlayerController.h"

#include "Kismet/GameplayStatics.h"

#include "UnrealSFASPlayerHUD.h"
#include "UnrealSFASPlayerWidget.h"

AUnrealSFASPlayerController::AUnrealSFASPlayerController()
    : bIsPause(false)
{}

void AUnrealSFASPlayerController::BeginPlay()
{
    // Shoe mouse and keep it locked on screen
    FInputModeGameAndUI InputMode;
    InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);
    InputMode.SetHideCursorDuringCapture(false);
    SetInputMode(InputMode);
    bShowMouseCursor = true;
}

void AUnrealSFASPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction("Esc", IE_Pressed, this, &AUnrealSFASPlayerController::TogglePause);
}

void AUnrealSFASPlayerController::TogglePause()
{
    // This will call the Game Mode to set the Global Time Dilation to 0 and stop everything in the game
    AUnrealSFASPlayerHUD* HUD = Cast<AUnrealSFASPlayerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
    if (HUD)
    {
        if (bIsPause == false)
        {
            HUD->PlayerWidget->EnablePauseMenu(true);
            bIsPause = true;
        }
        else
        {
            HUD->PlayerWidget->EnablePauseMenu(false);
            bIsPause = false;
        }
    }
}
